1. Скачиваем и настраиваем BitrixVM7.5.1 для HyperV

2. MySQL Exporter
	Скачиваем и распаковываем дистрибутив
		curl -s https://api.github.com/repos/prometheus/mysqld_exporter/releases/latest   | grep browser_download_url   | grep linux-amd64 | cut -d '"' -f 4   | wget -qi -
		tar xvf mysqld_exporter*.tar.gz
		sudo mv  mysqld_exporter-*.linux-amd64/mysqld_exporter /usr/local/bin/
		sudo chmod +x /usr/local/bin/mysqld_exporter
	
	На СУБД создаем пользователя и выдаем ему полные права
		mysql
		CREATE USER 'mysqld_exporter'@'localhost' IDENTIFIED BY 'passwd';
		GRANT PROCESS, REPLICATION CLIENT, SELECT ON *.* TO 'mysqld_exporter'@'localhost';
		FLUSH PRIVILEGES;
		EXIT
		
	Создание credentials file для подключения к БД
		nano /etc/.mysqld_exporter.cnf
		[client]
		user=mysqld_exporter
		password=passwd
		chown root:prometheus /etc/.mysqld_exporter.cnf
		
	Создаем пользователя для работы с экспортером 
		sudo groupadd --system prometheus
		sudo useradd -s /sbin/nologin --system -g prometheus prometheus
		sudo groupadd --system prometheus
		chown root:prometheus /etc/.mysqld_exporter.cnf
		
	Создаем файл модуля Systemd
		nano /etc/systemd/system/mysql_exporter.service
		
		[Unit]
		Description=Prometheus MySQL Exporter
		After=network.target
		User=prometheus
		Group=prometheus

		[Service]
		Type=simple
		Restart=always
		ExecStart=/usr/local/bin/mysqld_exporter \
		--config.my-cnf /etc/.mysqld_exporter.cnf \
		--collect.global_status \
		--collect.info_schema.innodb_metrics \
		--collect.auto_increment.columns \
		--collect.info_schema.processlist \
		--collect.binlog_size \
		--collect.info_schema.tablestats \
		--collect.global_variables \
		--collect.info_schema.query_response_time \
		--collect.info_schema.userstats \
		--collect.info_schema.tables \
		--collect.perf_schema.tablelocks \
		--collect.perf_schema.file_events \
		--collect.perf_schema.eventswaits \
		--collect.perf_schema.indexiowaits \
		--collect.perf_schema.tableiowaits \
		--collect.slave_status \
		--web.listen-address=:9104

		[Install]
		WantedBy=multi-user.target

	Перезапускаем службу
		sudo systemctl daemon-reload
		sudo systemctl enable mysql_exporter
		sudo systemctl start mysql_exporter
	
	Открываем порт 9104
		iptables -I INPUT -p tcp --dport 9104 -j ACCEPT
		
3. Node Exporter
	Скачиваем и распаковываем дистрибутив
		wget https://github.com/prometheus/node_exporter/releases/download/v1.6.1/node_exporter-1.6.1.linux-amd64.tar.gz
		tar zxvf node_exporter-1.6.1.linux-amd64.tar.gz && cd node_exporter-1.6.1.linux-amd64
		cp node_exporter /usr/local/bin/
		sudo chmod +x /usr/local/bin/node_exporter
	
	Создаем пользователя для работы с экспортером 
		useradd --no-create-home --shell /bin/false nodeusr
		chown -R nodeusr:nodeusr /usr/local/bin/node_exporter
		
	Создаем файл модуля Systemd
		nano /etc/systemd/system/node_exporter.service
		
		[Unit]
		Description=Node Exporter
		Wants=network-online.target
		After=network-online.target

		[Service]
		User=nodeusr
		Group=nodeusr
		Type=simple
		ExecStart=/usr/local/bin/node_exporter
		Restart=always
		RestartSec=3

		[Install]
		WantedBy=multi-user.target

	Перезапускаем службу
		sudo systemctl daemon-reload
		sudo systemctl enable node_exporter
		sudo systemctl start node_exporter
	
	Открываем порт 9104
		iptables -I INPUT -p tcp --dport 9100 -j ACCEPT
		
4. Nginx Exporter
	Скачиваем и распаковываем дистрибутив
		wget https://github.com/nginxinc/nginx-prometheus-exporter/releases/download/v0.11.0/nginx-prometheus-exporter_0.11.0_linux_amd64.tar.gz
		tar zxvf nginx-prometheus-exporter_0.11.0_linux_amd64.tar.gz && cd nginx-prometheus-exporter_0.11.0_linux_amd64
		cp nginx_prometheus_exporter /usr/local/bin/
		sudo chmod +x /usr/local/bin/nginx_prometheus_exporter
	
	В Nginx создаем конфиг для сбора статистики
		nano /etc/nginx/bx/site_enabled/nginx-status.conf
		
		server { 
			listen 127.0.0.1:8300; 
			location /nginx-status { 
				stub_status on; 
				access_log   off; 
				allow 127.0.0.1; 
				deny all; 
			} 
		}
	
	Добавляем файл с переменными
		nano /etc/default/nginx_prometheus_exporter
		
		ARGS=-nginx.scrape-uri http://127.0.0.1:8300/nginx-status
		
	***Создаем пользователя для работы с экспортером 
		useradd --no-create-home --shell /bin/false nodeusr
		chown -R nodeusr:nodeusr /usr/local/bin/nginx_prometheus_exporter
	
	Создаем файл модуля Systemd
		nano /etc/systemd/system/nginx_prometheus_exporter.service
		
		[Unit]
		Description=nginx_prometheus_exporter
		Requires=network.target
		After=network.target
		Documentation=

		[Service]
		Type=simple
		User=nodeusr
		Group=nodeusr
		StartLimitBurst=5
		StartLimitInterval=0
		Restart=on-failure
		RestartSec=1
		EnvironmentFile=/etc/default/nginx_prometheus_exporter
		ExecStart=/usr/local/bin/nginx-prometheus-exporter \
		$ARGS
		ExecStop=/bin/kill -s SIGTERM $MAINPID

		[Install]
		WantedBy=multi-user.target

	Перезапускаем службу
		sudo systemctl daemon-reload
		sudo systemctl enable node_exporter
		sudo systemctl start node_exporter
	
	Открываем порт 9113
		iptables -I INPUT -p tcp --dport 9113 -j ACCEPT
		
5. Backbox Exporter
	Скачиваем и распаковываем дистрибутив
		wget https://github.com/prometheus/blackbox_exporter/releases/download/v0.24.0/blackbox_exporter-0.24.0.linux-amd64.tar.gz
		tar xvzf blackbox_exporter-0.24.0.linux-amd64.tar.gz
		cd blackbox_exporter-0.24.0.linux-amd64
		mv blackbox_exporter /usr/local/bin
		sudo chmod +x /usr/local/bin/blackbox_exporter
		mkdir -p /etc/blackbox
		mv blackbox.yml /etc/blackbox
	
	***Создаем пользователя для работы с экспортером 
		useradd --no-create-home --shell /bin/false nodeusr
		chown -R nodeusr:nodeusr /usr/local/bin/nginx_prometheus_exporter
	Создаем файл модуля Systemd
		nano /etc/systemd/system/blackbox.service
		
		[Unit]
		Description=Blackbox Exporter Service
		Wants=network-online.target
		After=network-online.target

		[Service]
		Type=simple
		User=nodeusr
		Group=nodeusr
		ExecStart=/usr/local/bin/blackbox_exporter \
		  --config.file=/etc/blackbox/blackbox.yml \
		  --web.listen-address=:9115

		Restart=always

		[Install]
		WantedBy=multi-user.target


	Перезапускаем службу
		sudo systemctl daemon-reload
		sudo systemctl enable blackbox_exporter
		sudo systemctl start blackbox_exporter
	
	Открываем порт 9115
		iptables -I INPUT -p tcp --dport 9115 -j ACCEPT
		
6. PHP-FPM Exporter
	Скачиваем и распаковываем дистрибутив
		wget https://github.com/Lusitaniae/phpfpm_exporter/releases/download/v0.5.0/phpfpm_exporter-0.5.0.linux-amd64.tar.gz
		tar xvf phpfpm_exporter-0.5.0.linux-amd64.tar.gz
		cp phpfpm_exporter-0.5.0.linux-amd64/phpfpm_exporter /usr/local/bin/
	
	***Создаем пользователя для работы с экспортером 
		useradd --no-create-home --shell /bin/false nodeusr
		chown -R nodeusr:nodeusr /usr/local/bin/nginx_prometheus_exporter
		
	Перечисляем все сокеты fpm
		grep -ri "sock;" /etc/nginx/* | grep -v "#" |awk '{print $2,$3,$4}' | sed 's/fastcgi_pass unix://g' | sed 's/;//g' | sort -u
	Создаем файл модуля Systemd
		nano /etc/systemd/system/phpfpm_exporter.service
		
		[Unit]
		Description = PHP-FPM Prometheus Exporter

		[Service]
		SyslogIdentifier = phpfpm_exporter

		ExecStart =/usr/local/bin/phpfpm_exporter --phpfpm.socket-paths /var/run/phpfpm-api1.sock --phpfpm.socket-paths /var/run/phpfpm-api2-sock 


		[Install]
		WantedBy = multi-user.target


	Перезапускаем службу
		sudo systemctl daemon-reload
		sudo systemctl enable phpfpm_exporter
		sudo systemctl start phpfpm_exporter
	
	Открываем порт 9253
		iptables -I INPUT -p tcp --dport 9253 -j ACCEPT		
		

7. Запускаем через докер prometheus

	docker run -d -p 9090:9090 -v D:\DIT\GitLab\Исходники\monitoring\GAP-1\prometheus.yml:/etc/prometheus/prometheus.yml -v D:\DIT\GitLab\Исходники\monitoring\GAP-1\alert_rules.yml:/etc/prometheus/alert_rules.yml prom/prometheus

8. Запускаем через докер alertmanager
	
	docker run -d -p 9093:9093 --name prom_alertmanager -v D:\DIT\GitLab\Исходники\monitoring\GAP-1\alertmanager.yml:/etc/alertmanager/alertmanager.yml prom/alertmanager 

9. Запускаем через докер victoria-metrics

	docker build - < D:\DIT\GitLab\Исходники\monitoring\GAP-1\Dockerfile
	docker image ls
	docker run 66416372722f
	docker tag 66416372722f otus/victoria-metrics
	docker run -d -it --rm -v D:\DIT\GitLab\Исходники\monitoring\GAP-1\victoria-metrics-data:/victoria-metrics-data -p 8428:8428 otus/victoria-metrics

10. Запускаем через докер Grafana
	docker run -d -p 3000:3000 --name grafana grafana/grafana-enterprise:latest


