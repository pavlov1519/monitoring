apiVersion: v1
kind: PersistentVolume
metadata:
  name: grafana-pvc
  namespace: monitoring
spec:
  accessModes:
    - ReadWriteMany
  persistentVolumeReclaimPolicy: Retain
  storageClassName: nfs
  nfs:
    path: /data/grafana-pv
    server: 192.168.177.53
  capacity:
    storage: 1Gi

---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: grafana-pvc
  namespace: monitoring
spec:
  storageClassName: nfs
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 1Gi
  volumeName: grafana-pvc



helm upgrade --install --kube-context local -n monitoring grafana .\ -f .\values.yaml