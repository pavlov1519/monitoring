{{- define "name" -}}
{{- default .Chart.Name | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "labels" -}}
{{ include "selectorLabels" . }}
app.kubernetes.io/environment: {{ .Values.env }}
app.kubernetes.io/component: {{ .Values.tier }}
{{- end }}

{{- define "selectorLabels" -}}
app.kubernetes.io/app: {{ include "name" . }}
{{- end }}

{{- define "ingressAnnotations" -}}
kubernetes.io/ingress.class: {{ .Values.ingress.class }}
#nginx.ingress.kubernetes.io/rewrite-target: /
nginx.ingress.kubernetes.io/backend-protocol: "HTTP"
{{- end }}

{{- define "serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default .Chart.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- default "default" }}
{{- end }}
{{- end }}
