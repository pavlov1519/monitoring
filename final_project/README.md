#---------------------------------------------------------------------

## Тестовый стенд
Kubernetes:
Балансировщик haproxy и nginx 
kube-lb01.local

Control-plane
kube-cp01.local
kube-cp02.local

Workers
kube-worker01.local
kube-worker02.local

Сайт CMS Bitrix (BitrixVM7.5.5)
bitrixvm

Компоненты мониторинга
Grafana
Prometheus-stack
Victoria Metrics Alert
Victoria Metrics Single
Victoria Metrics Agent
Alertmanager

Компоненты логирования
Loki
Minio

Компоненты 


#---------------------------------------------------------------------

## Настройка балансировщика haproxy

# Установка набора обязательных утилит
yum install gcc openssl-devel readline-devel systemd-devel make pcre-devel tar lua lua-devel zlib-devel

# Установка LUA
wget --content-disposition --no-check-certificate  https://10.89.1.21/s/as8EdoYCKZczWBy/download
tar xvf lua-5.4.2.tar.gz
cd lua-5.4.2
make INSTALL_TOP=/opt/lua-5.4.2 linux install

# Установка HAproxy
wget --content-disposition --no-check-certificate  https://10.89.1.21/s/ZEN7SzodopHYZZj/download
cd ..
tar xvf haproxy.tar
cd haproxy
make TARGET=linux-glibc USE_LUA=1 USE_OPENSSL=1 USE_PCRE=1 USE_ZLIB=1 USE_SYSTEMD=1 USE_PROMEX=1 LUA_INC=/opt/lua-5.4.2/include LUA_LIB=/opt/lua-5.4.2/lib
make install-bin
cp /tmp/haproxy/haproxy /usr/sbin/haproxy
 
groupadd -g 188 haproxy
useradd -g 188 -u 188 -d /var/lib/haproxy -s /sbin/nologin -c haproxy haproxy
nano /etc/systemd/system/haproxy.service
 
[Unit]
Description=HAProxy
After=syslog.target network.target
 
[Service]
Type=notify
EnvironmentFile=/etc/sysconfig/haproxy
ExecStart=/usr/sbin/haproxy -f $CONFIG_FILE -p $PID_FILE $CLI_OPTIONS
ExecReload=/bin/kill -USR2 $MAINPID
ExecStop=/bin/kill -USR1 $MAINPID
 
[Install]
WantedBy=multi-user.target
 
nano /etc/sysconfig/haproxy
 
# Command line options to pass to HAProxy at startup
# The default is:
#CLI_OPTIONS="-Ws"
CLI_OPTIONS="-Ws"
 
# Specify an alternate configuration file. The default is:
#CONFIG_FILE=/etc/haproxy/haproxy.conf
CONFIG_FILE=/etc/haproxy/haproxy.cfg
 
# File used to track process IDs. The default is:
#PID_FILE=/var/run/haproxy.pid
PID_FILE=/var/run/haproxy.pid
 
systemctl daemon-reload
systemctl enable haproxy

# Пример конфига HAproxy
mkdir /etc/haproxy
nano /etc/haproxy/haproxy.cfg
 
global
        daemon
        log         127.0.0.1 local2     #Log configuration
        chroot      /var/lib/haproxy
        pidfile     /var/run/haproxy.pid
        maxconn     400000
        user        haproxy
        group       haproxy
        stats socket /var/lib/haproxy/stats
 
 
 
        ssl-default-bind-ciphers PROFILE=SYSTEM
        ssl-default-server-ciphers PROFILE=SYSTEM
 
#---------------------------------------------------------------------
# common defaults that all the 'listen' and 'backend' sections will
# use if not designated in their block
#---------------------------------------------------------------------
defaults
        mode                    http
        log                     global
        option                  httplog
        option                  dontlognull
        option http-server-close
        option forwardfor       except 127.0.0.0/8
        option                  redispatch
        retries                 1
        timeout http-request    10s
        timeout queue           20s
        timeout connect         15s
        timeout client          720s
        timeout server          720s
        timeout http-keep-alive 10s
        timeout check           20s
        maxconn                 100000
 
#---------------------------------------------------------------------
# frontend which proxys to the ingress controller
#---------------------------------------------------------------------
frontend k8s
    log global
    option tcplog
    mode http
    bind 0.0.0.0:8080
    stats enable
    stats uri /haproxy?stats
    stats auth admin:haproxystats
    option httpclose
    option forwardfor
    default_backend k8s
 
backend k8s
    mode http
#    http-check expect status 404
    balance leastconn
    option httpchk GET  /
    option redispatch
    retries 3
    server << Worker NODE >>  IP:80 check inter 10000 fall 2
 
frontend stats
   bind *:8404
#   option http-use-htx
   http-request use-service prometheus-exporter if { path /metrics }
   stats enable
   stats uri /stats
   stats refresh 10s
 
#---------------------------------------------------------------------
# apiserver frontend which proxys to the control plane nodes
#---------------------------------------------------------------------
frontend apiserver
    bind *:6443
    mode tcp
    option tcplog
    default_backend apiserver
 
#---------------------------------------------------------------------
# round robin balancing for api server
#---------------------------------------------------------------------
backend apiserver
    option httpchk GET /healthz
    http-check expect status 200
    mode tcp
    option ssl-hello-chk
    balance     roundrobin
        server << MASTER NODES >> IP:6443 check#


#---------------------------------------------------------------------

## Установка и настройка Kubernetes

# Редактируем hosts
cat <<EOF >> /etc/hosts
192.168.177.50 kube-lb01
192.168.177.51 kube-cp01
192.168.177.52 kube-cp02
192.168.177.53 kube-worker01
192.168.177.54 kube-worker02
192.168.177.55 bitrixvm
EOF

# Отключаем SELinux
sed -i "s/SELINUX=enforcing/SELINUX=permissive/" /etc/selinux/config
setenforce 0

# Отключаем SWAP на всех нодах
swapoff -a
sed -i '/swap/d' /etc/fstab

# Установка зависимостей
yum install -y conntrack runc

# Делаем предварительные настройки на всех нодах
lsmod | grep br_netfilter
lsmod | grep overlay
 
modprobe overlay
modprobe br_netfilter
 
cat <<EOF | tee /etc/modules-load.d/k8s.conf
overlay
br_netfilter
EOF
 
cat <<EOF | sudo tee /etc/sysctl.d/99-kubernetes-cri.conf
net.bridge.bridge-nf-call-iptables  = 1
net.ipv4.ip_forward                 = 1
net.bridge.bridge-nf-call-ip6tables = 1
EOF
 
echo "fs.inotify.max_user_instances=512" >> /etc/sysctl.conf
echo "fs.inotify.max_user_watches=262144" >> /etc/sysctl.conf
 
sysctl --system


# Установка containerd (необходимо установить на всех нодах)
curl -L "https://github.com/containerd/containerd/releases/download/v1.6.20/containerd-1.6.20-linux-amd64.tar.gz" | sudo tar -C /usr -xz


cat <<EOF | tee /usr/lib/systemd/system/containerd.service
[Unit]
Description=containerd container runtime
Documentation=https://containerd.io
After=network.target local-fs.target
 
[Service]
ExecStartPre=-/sbin/modprobe overlay
ExecStart=/usr/bin/containerd
 
Type=notify
Delegate=yes
KillMode=process
Restart=always
RestartSec=5
# Having non-zero Limit*s causes performance problems due to accounting overhead
# in the kernel. We recommend using cgroups to do container-local accounting.
LimitNPROC=infinity
LimitCORE=infinity
LimitNOFILE=infinity
# Comment TasksMax if your systemd version does not supports it.
# Only systemd 226 and above support this version.
TasksMax=infinity
OOMScoreAdjust=-999
 
[Install]
WantedBy=multi-user.target
EOF

mkdir -p /etc/containerd
/usr/bin/containerd config default | sudo tee /etc/containerd/config.toml
sed -i 's|SystemdCgroup = false|SystemdCgroup = true|g' /etc/containerd/config.toml
systemctl daemon-reload
systemctl enable --now containerd

# Устанавливаем CNI plugins
CNI_VERSION="v1.2.0"
mkdir -p /opt/cni/bin
curl -L "https://github.com/containernetworking/plugins/releases/download/${CNI_VERSION}/cni-plugins-linux-amd64-${CNI_VERSION}.tgz" | tar -C /opt/cni/bin -xz

# Указываем куда качать бинарники
DOWNLOAD_DIR=/usr/local/bin
sudo mkdir -p $DOWNLOAD_DIR

# Устанавливаем crictl (required for kubeadm / Kubelet Container Runtime Interface (CRI))
CRICTL_VERSION="v1.25.0"
ARCH="amd64"
curl -L "https://github.com/kubernetes-incubator/cri-tools/releases/download/${CRICTL_VERSION}/crictl-${CRICTL_VERSION}-linux-amd64.tar.gz" | sudo tar -C $DOWNLOAD_DIR -xz

# Устанавливаем kubeadm, kubelet, kubectl and add a kubelet systemd service:
RELEASE="v1.26.3" 
cd $DOWNLOAD_DIR
curl -L --remote-name-all  https://storage.googleapis.com/kubernetes-release/release/${RELEASE}/bin/linux/amd64/{kubeadm,kubelet,kubectl}
chmod +x {kubeadm,kubelet,kubectl}
 
RELEASE_VERSION="v0.4.0"
curl -sSL "https://raw.githubusercontent.com/kubernetes/release/v0.4.0/cmd/kubepkg/templates/latest/deb/kubelet/lib/systemd/system/kubelet.service" | sed "s:/usr/bin:${DOWNLOAD_DIR}:g" | sudo tee /etc/systemd/system/kubelet.service
mkdir -p /etc/systemd/system/kubelet.service.d
curl -sSL "https://raw.githubusercontent.com/kubernetes/release/v0.4.0/cmd/kubepkg/templates/latest/deb/kubeadm/10-kubeadm.conf" | sed "s:/usr/bin:${DOWNLOAD_DIR}:g" | sudo tee /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
 
systemctl enable --now kubelet


# Настройка crictl
echo -e "runtime-endpoint: unix:///var/run/containerd/containerd.sock\nimage-endpoint: unix:///var/run/containerd/containerd.sock\ntimeout: 10\ndebug: true" > /etc/crictl.yaml

# Рекомендуется перезагрузить систему
shutdown -r now


# Инициализация кластера
kubeadm config images pull
kubeadm init --pod-network-cidr=172.16.0.0/16 --control-plane-endpoint kubelb-01:6443 --upload-certs

# Установка calico
kubectl apply -f calico.yaml

# Установка ingress
kubectl apply -f ./ingress2.yaml

# Для сбора метрик с ETCD, kube-controller-manager и kube-scheduler metrics необходимо на всех control-plane нодах:
В конфиге /etc/kubernetes/manifests/etcd.yaml установить значение:
- --listen-metrics-urls=http://0.0.0.0:2381

В конфигах /etc/kubernetes/manifests/kube-controller-manager.yaml и /etc/kubernetes/manifests/kube-scheduler.yaml установить значение:
- --bind-address=0.0.0.0

# Для сбора метрик с kube-proxy необходимо:
Изменить конфигурацию kube-proxy:
kubectl edit cm -n kube-system kube-proxy
metricsBindAddress: 0.0.0.0


#---------------------------------------------------------------------
# Установка Exporter
# MySQL Exporter
Скачиваем и распаковываем дистрибутив
    curl -s https://api.github.com/repos/prometheus/mysqld_exporter/releases/latest   | grep browser_download_url   | grep linux-amd64 | cut -d '"' -f 4   | wget -qi -
    tar xvf mysqld_exporter*.tar.gz
    sudo mv  mysqld_exporter-*.linux-amd64/mysqld_exporter /usr/local/bin/
    sudo chmod +x /usr/local/bin/mysqld_exporter

На СУБД создаем пользователя и выдаем ему полные права
    mysql
    CREATE USER 'mysqld_exporter'@'localhost' IDENTIFIED BY 'passwd';
    GRANT PROCESS, REPLICATION CLIENT, SELECT ON *.* TO 'mysqld_exporter'@'localhost';
    FLUSH PRIVILEGES;
    EXIT
    
Создание credentials file для подключения к БД
    nano /etc/.mysqld_exporter.cnf
    [client]
    user=mysqld_exporter
    password=passwd
    chown root:prometheus /etc/.mysqld_exporter.cnf
    
Создаем пользователя для работы с экспортером 
    sudo groupadd --system prometheus
    sudo useradd -s /sbin/nologin --system -g prometheus prometheus
    sudo groupadd --system prometheus
    chown root:prometheus /etc/.mysqld_exporter.cnf
    
Создаем файл модуля Systemd
    nano /etc/systemd/system/mysql_exporter.service
    
    [Unit]
    Description=Prometheus MySQL Exporter
    After=network.target
    User=prometheus
    Group=prometheus

    [Service]
    Type=simple
    Restart=always
    ExecStart=/usr/local/bin/mysqld_exporter \
    --config.my-cnf /etc/.mysqld_exporter.cnf \
    --collect.global_status \
    --collect.info_schema.innodb_metrics \
    --collect.auto_increment.columns \
    --collect.info_schema.processlist \
    --collect.binlog_size \
    --collect.info_schema.tablestats \
    --collect.global_variables \
    --collect.info_schema.query_response_time \
    --collect.info_schema.userstats \
    --collect.info_schema.tables \
    --collect.perf_schema.tablelocks \
    --collect.perf_schema.file_events \
    --collect.perf_schema.eventswaits \
    --collect.perf_schema.indexiowaits \
    --collect.perf_schema.tableiowaits \
    --collect.slave_status \
    --web.listen-address=:9104

    [Install]
    WantedBy=multi-user.target

Перезапускаем службу
    sudo systemctl daemon-reload
    sudo systemctl enable mysql_exporter
    sudo systemctl start mysql_exporter

Открываем порт 9104
    iptables -I INPUT -p tcp --dport 9104 -j ACCEPT

# Node Exporter
Скачиваем и распаковываем дистрибутив
    wget https://github.com/prometheus/node_exporter/releases/download/v1.6.1/node_exporter-1.6.1.linux-amd64.tar.gz
    tar zxvf node_exporter-1.6.1.linux-amd64.tar.gz && cd node_exporter-1.6.1.linux-amd64
    cp node_exporter /usr/local/bin/
    sudo chmod +x /usr/local/bin/node_exporter

Создаем пользователя для работы с экспортером 
    useradd --no-create-home --shell /bin/false nodeusr
    chown -R nodeusr:nodeusr /usr/local/bin/node_exporter
    
Создаем файл модуля Systemd
    nano /etc/systemd/system/node_exporter.service
    
    [Unit]
    Description=Node Exporter
    Wants=network-online.target
    After=network-online.target

    [Service]
    User=nodeusr
    Group=nodeusr
    Type=simple
    ExecStart=/usr/local/bin/node_exporter
    Restart=always
    RestartSec=3

    [Install]
    WantedBy=multi-user.target

Перезапускаем службу
    sudo systemctl daemon-reload
    sudo systemctl enable node_exporter
    sudo systemctl start node_exporter

Открываем порт 9104
    iptables -I INPUT -p tcp --dport 9100 -j ACCEPT

# Nginx Exporter
Скачиваем и распаковываем дистрибутив
    wget https://github.com/nginxinc/nginx-prometheus-exporter/releases/download/v0.11.0/nginx-prometheus-exporter_0.11.0_linux_amd64.tar.gz
    tar zxvf nginx-prometheus-exporter_0.11.0_linux_amd64.tar.gz && cd nginx-prometheus-exporter_0.11.0_linux_amd64
    cp nginx_prometheus_exporter /usr/local/bin/
    sudo chmod +x /usr/local/bin/nginx_prometheus_exporter

В Nginx создаем конфиг для сбора статистики
    nano /etc/nginx/bx/site_enabled/nginx-status.conf
    
    server { 
        listen 127.0.0.1:8300; 
        location /nginx-status { 
            stub_status on; 
            access_log   off; 
            allow 127.0.0.1; 
            deny all; 
        } 
    }

Добавляем файл с переменными
    nano /etc/default/nginx_prometheus_exporter
    
    ARGS=-nginx.scrape-uri http://127.0.0.1:8300/nginx-status
    
***Создаем пользователя для работы с экспортером 
    useradd --no-create-home --shell /bin/false nodeusr
    chown -R nodeusr:nodeusr /usr/local/bin/nginx_prometheus_exporter

Создаем файл модуля Systemd
    nano /etc/systemd/system/nginx_prometheus_exporter.service
    
    [Unit]
    Description=nginx_prometheus_exporter
    Requires=network.target
    After=network.target
    Documentation=

    [Service]
    Type=simple
    User=nodeusr
    Group=nodeusr
    StartLimitBurst=5
    StartLimitInterval=0
    Restart=on-failure
    RestartSec=1
    EnvironmentFile=/etc/default/nginx_prometheus_exporter
    ExecStart=/usr/local/bin/nginx-prometheus-exporter \
    $ARGS
    ExecStop=/bin/kill -s SIGTERM $MAINPID

    [Install]
    WantedBy=multi-user.target

Перезапускаем службу
    sudo systemctl daemon-reload
    sudo systemctl enable nginx_prometheus_exporter.service
    sudo systemctl start nginx_prometheus_exporter.service

Открываем порт 9113
    iptables -I INPUT -p tcp --dport 9113 -j ACCEPT

# Backbox Exporter
Скачиваем и распаковываем дистрибутив
    wget https://github.com/prometheus/blackbox_exporter/releases/download/v0.24.0/blackbox_exporter-0.24.0.linux-amd64.tar.gz
    tar xvzf blackbox_exporter-0.24.0.linux-amd64.tar.gz
    cd blackbox_exporter-0.24.0.linux-amd64
    mv blackbox_exporter /usr/local/bin
    sudo chmod +x /usr/local/bin/blackbox_exporter
    mkdir -p /etc/blackbox
    mv blackbox.yml /etc/blackbox

***Создаем пользователя для работы с экспортером 
    useradd --no-create-home --shell /bin/false nodeusr
    chown -R nodeusr:nodeusr /usr/local/bin/nginx_prometheus_exporter
Создаем файл модуля Systemd
    nano /etc/systemd/system/blackbox.service
    
    [Unit]
    Description=Blackbox Exporter Service
    Wants=network-online.target
    After=network-online.target

    [Service]
    Type=simple
    User=nodeusr
    Group=nodeusr
    ExecStart=/usr/local/bin/blackbox_exporter \
        --config.file=/etc/blackbox/blackbox.yml \
        --web.listen-address=:9115

    Restart=always

    [Install]
    WantedBy=multi-user.target


Перезапускаем службу
    sudo systemctl daemon-reload
    sudo systemctl enable blackbox_exporter
    sudo systemctl start blackbox_exporter

Открываем порт 9115
    iptables -I INPUT -p tcp --dport 9115 -j ACCEPT
    
# PHP-FPM Exporter
Скачиваем и распаковываем дистрибутив
    wget https://github.com/Lusitaniae/phpfpm_exporter/releases/download/v0.5.0/phpfpm_exporter-0.5.0.linux-amd64.tar.gz
    tar xvf phpfpm_exporter-0.5.0.linux-amd64.tar.gz
    cp phpfpm_exporter-0.5.0.linux-amd64/phpfpm_exporter /usr/local/bin/

***Создаем пользователя для работы с экспортером 
    useradd --no-create-home --shell /bin/false nodeusr
    chown -R nodeusr:nodeusr /usr/local/bin/nginx_prometheus_exporter
    
Перечисляем все сокеты fpm
    grep -ri "sock;" /etc/nginx/* | grep -v "#" |awk '{print $2,$3,$4}' | sed 's/fastcgi_pass unix://g' | sed 's/;//g' | sort -u
Создаем файл модуля Systemd
    nano /etc/systemd/system/phpfpm_exporter.service
    
    [Unit]
    Description = PHP-FPM Prometheus Exporter

    [Service]
    SyslogIdentifier = phpfpm_exporter

    ExecStart =/usr/local/bin/phpfpm_exporter --phpfpm.socket-paths /var/run/phpfpm-api1.sock --phpfpm.socket-paths /var/run/phpfpm-api2-sock 


    [Install]
    WantedBy = multi-user.target


Перезапускаем службу
    sudo systemctl daemon-reload
    sudo systemctl enable phpfpm_exporter
    sudo systemctl start phpfpm_exporter

Открываем порт 9253
    iptables -I INPUT -p tcp --dport 9253 -j ACCEPT		

#---------------------------------------------------------------------
## Установка компонентов мониторинга



# Grafana
Создание PV PVC
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: grafana-pvc
  namespace: monitoring
spec:
  accessModes:
    - ReadWriteMany
  persistentVolumeReclaimPolicy: Retain
  storageClassName: nfs
  nfs:
    path: /data/grafana-pv
    server: 192.168.177.53
  capacity:
    storage: 1Gi

---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: grafana-pvc
  namespace: monitoring
spec:
  storageClassName: nfs
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 1Gi
  volumeName: grafana-pvc


helm upgrade --install --kube-context local -n monitoring grafana .\grafana -f .\grafana\values.yaml



# Prometheus-stack
# Install
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
helm upgrade -i --kube-context local --set prometheus.prometheusSpec.externalLabels.cluster=prod -n monitoring prometheus-stack prometheus-community/kube-prometheus-stack -f .\prometheus-stack\values.yml

# Uninstall
helm uninstall --kube-context local -n monitoring prometheus-stack



# Victoria Metrics Alert
# Install
helm repo add vm https://victoriametrics.github.io/helm-charts/
helm repo update
helm upgrade -i --kube-context local -n monitoring vmalert vm/victoria-metrics-alert -f .\victoriametrics\values.yml

# Uninstall
helm uninstall --kube-context local -n monitoring vmalert



# Victoria Metrics Single
# Install
helm upgrade -i --kube-context local -n monitoring vmsingle vm/victoria-metrics-single -f .\victoria-metrics-single\values.yaml

# Uninstall
helm uninstall --kube-context local -n monitoring vmsingle



# Victoria Metrics Agent
# Install
#helm repo add vm https://victoriametrics.github.io/helm-charts/
#helm repo update
helm upgrade --install --kube-context local -n monitoring vmagent .\ -f .\victoria-metrics-agent\values.yaml

# Uninstall
helm uninstall --kube-context local -n monitoring vmagent



# Alertmanager
# Install
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
helm upgrade -i --kube-context local -n monitoring alertmanager prometheus-community/alertmanager -f .\alertmanager\values.yaml

# Uninstall
helm uninstall --kube-context local -n monitoring alertmanager

#---------------------------------------------------------------------

## Установка компонентов логирования



# Loki
# Install
helm upgrade -i --kube-context mesmon -n loki loki .\loki-distributed -f .\loki-distributed\ci\minio-values.yaml

# Uninstall
helm uninstall --kube-context local -n monitoring loki



# Minio
# Install
kubectl apply -f .\minio\templates --context local -n monitoring

